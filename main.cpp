#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <sstream>
#include <vector>

using namespace std;

int main()
{
    std::string url;
    std::vector<std::string> url_vector;
    std::ofstream OutFile("defang_url.txt");

    // accept an input file 

    std::cout << "Enter a url to defang: ";
    std::cin >> url;
    url_vector.push_back(url);
    

    
    for (auto i : url_vector)
    {
        // replace "http" -> "hxxp"
        std::replace(i.begin(), i.end(), 't', 'x');
        std::stringstream ss(i);
        for (int j = 0; j < i.length(); j++)
        {
            if (i[j] == '.')
            {
                ss << "[.]";
            }
            else
            {
                ss << i[j];
            }
        }
        OutFile << ss.str() + '\n';
    }
    return 0;
}